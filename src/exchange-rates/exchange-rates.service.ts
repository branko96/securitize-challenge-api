import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateExchangeRateDto } from './dto/exchangeRates.dto';
import { ExchangeRate } from './interfaces/exchangeRates.interface';

@Injectable()
export class ExchangeRatesService {
  constructor(
    @InjectModel('ExchangeRate')
    private readonly exchangeRateModel: Model<ExchangeRate>,
  ) {}

  async getExchangeRates(): Promise<ExchangeRate[]> {
    return this.exchangeRateModel.find();
  }

  async createExchangeRate(
    createExchangeRateDto: CreateExchangeRateDto,
  ): Promise<ExchangeRate> {
    const createdExchangeRate = new this.exchangeRateModel(
      createExchangeRateDto,
    );
    return await createdExchangeRate.save();
  }

  async updateExchangeRate(
    exchangeRateId: string,
    createExchangeRateDto: CreateExchangeRateDto,
  ): Promise<ExchangeRate> {
    return this.exchangeRateModel.findByIdAndUpdate(
      exchangeRateId,
      createExchangeRateDto,
      { new: true },
    );
  }
}
