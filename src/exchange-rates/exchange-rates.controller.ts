import {
  Body,
  Controller,
  Get,
  HttpStatus,
  NotFoundException,
  Param,
  Post,
  Put,
  Res,
} from '@nestjs/common';
import { CreateExchangeRateDto } from './dto/exchangeRates.dto';
import { ExchangeRatesService } from './exchange-rates.service';

@Controller('exchange-rates')
export class ExchangeRatesController {
  constructor(private exchangeRatesService: ExchangeRatesService) {}

  @Post('/create')
  async createExchangeRate(
    @Res() res,
    @Body() createExchangeRateDto: CreateExchangeRateDto,
  ) {
    const createdExchangeRate =
      await this.exchangeRatesService.createExchangeRate(createExchangeRateDto);
    console.log(createdExchangeRate);
    return res.status(HttpStatus.OK).json({
      message: 'Exchange Rate has been submitted successfully!',
      data: createdExchangeRate,
    });
  }

  @Get('/')
  async getExchangeRates(@Res() res) {
    const exchangeRates = await this.exchangeRatesService.getExchangeRates();
    console.log(exchangeRates);
    return res.status(HttpStatus.OK).json({
      data: exchangeRates,
    });
  }

  @Put('/:exchangeRateId')
  async updateExchangeRate(
    @Res() res,
    @Param('exchangeRateId') exchangeRateId,
    @Body() createExchangeRateDto: CreateExchangeRateDto,
  ) {
    const updatedExchangeRate =
      await this.exchangeRatesService.updateExchangeRate(
        exchangeRateId,
        createExchangeRateDto,
      );
    if (!updatedExchangeRate)
      throw new NotFoundException('Exchange Rate does not exist!');
    return res.status(HttpStatus.OK).json({
      message: 'Exchange Rate has been successfully updated',
      data: updatedExchangeRate,
    });
  }
}
