import { Document } from 'mongoose';

export interface ExchangeRate extends Document {
  readonly currency: string;
  readonly value: number;
  readonly createdAt: Date;
}
