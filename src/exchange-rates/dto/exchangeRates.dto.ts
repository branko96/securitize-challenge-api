export class CreateExchangeRateDto {
  readonly value: number;
  readonly currency: string;
}
