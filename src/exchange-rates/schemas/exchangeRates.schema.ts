import { Schema } from 'mongoose';

const exchangeRatesSchema = new Schema({
  currency: {
    type: String,
    required: true,
  },
  value: {
    type: Number,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

export default exchangeRatesSchema;
