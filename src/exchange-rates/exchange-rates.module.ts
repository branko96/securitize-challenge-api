import { Module } from '@nestjs/common';
import { ExchangeRatesService } from './exchange-rates.service';
import { ExchangeRatesController } from './exchange-rates.controller';
import { MongooseModule } from '@nestjs/mongoose';
import exchangeRatesSchema from './schemas/exchangeRates.schema';
import { ExchangeRatesSeed } from './seeds/exchangeRates.seed';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'ExchangeRate', schema: exchangeRatesSchema },
    ]),
  ],
  providers: [ExchangeRatesService, ExchangeRatesSeed],
  controllers: [ExchangeRatesController],
})
export class ExchangeRatesModule {}
