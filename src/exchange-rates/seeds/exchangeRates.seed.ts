import { Command } from 'nestjs-command';
import { Injectable } from '@nestjs/common';

import { ExchangeRatesService } from '../exchange-rates.service';

@Injectable()
export class ExchangeRatesSeed {
  constructor(private readonly exchangeRatesService: ExchangeRatesService) {}

  @Command({
    command: 'create:rates',
    describe: 'create usd and euro rates',
  })
  async create() {
    await this.exchangeRatesService.createExchangeRate({
      currency: 'USD',
      value: 3000,
    });
    await this.exchangeRatesService.createExchangeRate({
      currency: 'EUR',
      value: 2746,
    });
  }
}
