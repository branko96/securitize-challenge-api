import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { WalletModule } from './wallet/wallet.module';
import { ExchangeRatesModule } from './exchange-rates/exchange-rates.module';
import { SeedsModule } from './shared/seeds.module';
import { CommandModule } from 'nestjs-command';

@Module({
  imports: [
    CommandModule,
    WalletModule,
    MongooseModule.forRoot('mongodb://localhost/wallet-dashboard'),
    ExchangeRatesModule,
  ],
  controllers: [AppController],
  providers: [AppService, SeedsModule],
})
export class AppModule {}
