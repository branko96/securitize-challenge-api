import { Module } from '@nestjs/common';
import { CommandModule } from 'nestjs-command';

import { ExchangeRatesSeed } from '../exchange-rates/seeds/exchangeRates.seed';

@Module({
  imports: [CommandModule],
  providers: [ExchangeRatesSeed],
  exports: [ExchangeRatesSeed],
})
export class SeedsModule {}
