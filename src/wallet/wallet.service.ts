import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Wallet } from './interfaces/wallet.interface';
import { InjectModel } from '@nestjs/mongoose';
import { CreateWalletDto } from './dto/wallet.dto';

@Injectable()
export class WalletService {
  constructor(
    @InjectModel('Wallet') private readonly walletModel: Model<Wallet>,
  ) {}

  async getWallets(orderBy): Promise<Wallet[]> {
    if (orderBy && orderBy === 'favorite') {
      return this.walletModel.find().sort({ favorite: 'desc' });
    }
    return this.walletModel.find().sort({ favorite: 'asc' });
  }

  async getWallet(walletId: string): Promise<Wallet> {
    const wallet = await this.walletModel.findById(walletId);
    return wallet;
  }

  async createWallet(createWalletDto: CreateWalletDto): Promise<Wallet> {
    const createdWallet = new this.walletModel(createWalletDto);
    return await createdWallet.save();
  }

  async updateWallet(
    walletId: string,
    createWalletDto: CreateWalletDto,
  ): Promise<Wallet> {
    const updatedWallet = await this.walletModel.findByIdAndUpdate(
      walletId,
      createWalletDto,
      { new: true },
    );
    return updatedWallet;
  }
}
