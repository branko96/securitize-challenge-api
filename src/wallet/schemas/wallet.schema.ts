import { Schema } from 'mongoose';

const walletSchema = new Schema({
  address: {
    type: String,
    required: true,
  },
  favorite: {
    type: Boolean,
    required: false,
    default: false,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

export default walletSchema;
