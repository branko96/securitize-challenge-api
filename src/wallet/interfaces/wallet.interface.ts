import { Document } from 'mongoose';

export interface Wallet extends Document {
  readonly address: string;
  readonly favorite: boolean;
  readonly createdAt: Date;
}
