export class CreateWalletDto {
  readonly address: string;
  readonly favorite: boolean;
  readonly createdAt: Date;
}
