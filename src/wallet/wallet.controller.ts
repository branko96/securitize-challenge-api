import {
  Controller,
  Post,
  Res,
  HttpStatus,
  Body,
  Get,
  Put,
  Param,
  NotFoundException,
  Query,
} from '@nestjs/common';

import { CreateWalletDto } from './dto/wallet.dto';
import { WalletService } from './wallet.service';

@Controller('wallet')
export class WalletController {
  constructor(private walletService: WalletService) {}

  @Post('/create')
  async createProduct(@Res() res, @Body() createProductDto: CreateWalletDto) {
    const wallet = await this.walletService.createWallet(createProductDto);
    return res.status(HttpStatus.OK).json({
      message: 'Wallet has been submitted successfully!',
      data: wallet,
    });
  }

  @Get('/')
  async getWallets(@Res() res, @Query('orderBy') orderBy: string) {
    const wallets = await this.walletService.getWallets(orderBy);
    return res.status(HttpStatus.OK).json({
      data: wallets,
    });
  }

  @Get('/:walletId')
  async getWallet(@Res() res, @Param('walletId') walletId) {
    console.log('walletId');
    const wallet = await this.walletService.getWallet(walletId);
    if (!wallet) throw new NotFoundException('Wallet does not exist!');
    return res.status(HttpStatus.OK).json({
      data: wallet,
    });
  }

  @Put('/:walletId')
  async updateWallet(
    @Res() res,
    @Param('walletId') walletId,
    @Body() createWalletDto: CreateWalletDto,
  ) {
    const updatedWallet = await this.walletService.updateWallet(
      walletId,
      createWalletDto,
    );
    if (!updatedWallet) throw new NotFoundException('Wallet does not exist!');
    return res.status(HttpStatus.OK).json({
      message: 'Wallet has been successfully updated',
      data: updatedWallet,
    });
  }
}
